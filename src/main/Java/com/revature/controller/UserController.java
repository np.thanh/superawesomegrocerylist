package com.revature.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.revature.models.Response;
import com.revature.models.User;
import com.revature.services.UserService;
import com.revature.services.UserServiceImpl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.stream.Collectors;

//singleton
public class UserController {
    private static UserController userController;
    UserService userService;
    private UserController(){
        userService = new UserServiceImpl();
    }

    public static UserController getInstance(){
        if(userController == null)
            userController = new UserController();

        return userController;
    }

    public void login(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json");
        PrintWriter out = resp.getWriter();

        String requestBody = req.getReader().lines().collect(Collectors.joining(System.lineSeparator()));

        User user = new ObjectMapper().readValue(requestBody,User.class);

        User tempUser = userService.login(user);

        if(tempUser != null){
            out.println(new ObjectMapper().writeValueAsString(new Response("login successful", true, tempUser)));
        }else{
            out.println(new ObjectMapper().writeValueAsString(new Response("invalid username or password", false, null)));
        }

    }
}
