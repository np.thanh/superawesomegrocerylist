package com.revature.dao;

import com.revature.models.User;

public interface UserDao {
    void insertUser(User user);
    User getOneUser(String username);
}
