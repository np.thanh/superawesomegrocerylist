package com.revature.dao;

import com.revature.models.GroceryList;
import com.revature.models.Item;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
//singleton
public class ItemDaoImpl implements ItemDao{
    private static ItemDao itemDao;

    private ItemDaoImpl(){
        try{
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public static ItemDao getInstance(){
        if(itemDao == null){
            itemDao = new ItemDaoImpl();
        }

        return itemDao;
    }

    @Override
    public void createItem(Item item) {
        try(Connection conn = DriverManager.getConnection(ConnectionUtil.url,ConnectionUtil.username,ConnectionUtil.password)){
            String sql = "INSERT INTO items VALUES (DEFAULT, ?, DEFAULT, ?)";

            PreparedStatement ps = conn.prepareStatement(sql);

            //we need this line to fill the ? input
            ps.setString(1, item.getName());
            ps.setInt(2, item.getListFk());


            ps.executeUpdate();


        }catch(SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Item> getAllItemsGivenListId(Integer listId) {
        List<Item> items = new ArrayList<>();

        try(Connection conn = DriverManager.getConnection(ConnectionUtil.url,ConnectionUtil.username,ConnectionUtil.password)){

            String sql = "SELECT * FROM items WHERE list_id_fk = ?";

            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setInt(1, listId);

            //this is wh
            ResultSet rs = ps.executeQuery();


            //this is iterating through the records
            while(rs.next()) {
                items.add(
                    new Item(rs.getInt(1),rs.getString(2),rs.getBoolean(3),rs.getInt(4))
                );
            }

        }catch(SQLException e) {
            e.printStackTrace();
        }

        return items;
    }

    @Override
    public void deleteItem(Integer itemId) {
        try(Connection conn = DriverManager.getConnection(ConnectionUtil.url,ConnectionUtil.username,ConnectionUtil.password)){
            String sql = "DELETE FROM items WHERE item_id = ?";

            PreparedStatement ps = conn.prepareStatement(sql);

            //we need this line to fill the ? input
            ps.setInt(1, itemId);

            ps.executeUpdate();


        }catch(SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void markItemInCart(Integer itemId) {
        try(Connection conn = DriverManager.getConnection(ConnectionUtil.url,ConnectionUtil.username,ConnectionUtil.password)){
            String sql = "UPDATE items SET in_cart = TRUE WHERE item_id = ?";

            PreparedStatement ps = conn.prepareStatement(sql);

            //we need this line to fill the ? input
            ps.setInt(1, itemId);

            ps.executeUpdate();


        }catch(SQLException e) {
            e.printStackTrace();
        }
    }
}
