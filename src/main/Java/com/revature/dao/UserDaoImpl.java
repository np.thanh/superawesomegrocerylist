package com.revature.dao;

import com.revature.models.User;

import java.sql.*;

//singleton
public class UserDaoImpl implements UserDao{
    private static UserDao userDao;

    private UserDaoImpl(){
        try{
            Class.forName("org.postgresql.Driver");
        }catch(ClassNotFoundException e){
            e.printStackTrace();
        }
    }

    public static UserDao getInstance(){
        if(userDao == null){
            userDao = new UserDaoImpl();
        }

        return userDao;
    }

    @Override
    public void insertUser(User user) {
        try(Connection conn = DriverManager.getConnection(ConnectionUtil.url,ConnectionUtil.username,ConnectionUtil.password)){
            String sql = "INSERT INTO users VALUES (DEFAULT, ?,?,DEFAULT);";

            PreparedStatement ps = conn.prepareStatement(sql);

            //we need this line to fill the ? input
            ps.setString(1, user.getUsername());
            ps.setString(2,user.getPassword());


            ps.executeUpdate();


        }catch(SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public User getOneUser(String username) {
        User user = null;

        try(Connection conn = DriverManager.getConnection(ConnectionUtil.url,ConnectionUtil.username,ConnectionUtil.password)){

            String sql = "SELECT * FROM users WHERE username = ?";

            PreparedStatement ps = conn.prepareStatement(sql);

            ps.setString(1, username);

            //this is wh
            ResultSet rs = ps.executeQuery();


            //this is iterating through the records
            while(rs.next()) {
                user = new User(rs.getInt(1),rs.getString(2),rs.getString(3),rs.getDate(4));
            }

        }catch(SQLException e) {
            e.printStackTrace();
        }

        return user;
    }
}
